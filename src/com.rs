use std::collections::HashMap;
use std::io::Write;

use crate::lib::Lexer; 
use crate::lib::Op;
use crate::lib::Kind;
use crate::lib::Sv;
use crate::lib::Atom;

#[derive(Debug,Clone)]
pub enum Expr {
    Stackv {
        id: u64,
        val : Sv
    },

    Unary {
        id: u64,
        op  : Op,
    },

    Binary {
        id: u64,
        op  : Op,
    },

    If {
        ifs : Box<Expr>,
        els : Box<Expr>,
    },  
    
    Repeat {
        body : Box<Expr>,
    },

    FuncDef {
        id: u64,
        name: String,
        args: Box<Expr>,
        body: Box<Expr>,
    },

    Append {
        id: u64,
        var: Box<Expr>,
    },

    Pop {
        id: u64,
        var: Box<Expr>,
    },

    Store {
        id: u64,
        var: Box<Expr>,
    },

    Id {
        id: u64,
        var: Box<Expr>,
    },

    StackFrame {
        id: u64,
        val : Vec<Expr>,
    },

    ArgFrame {
        id: u64,
        lhv : Vec<String>,
    },

    FuncCall {
        id: u64,
        name: String,
        args: Vec<Expr>,
    },
    FuncBody {
        body: Box<Expr>,
    }
}


pub fn parse(values: Vec<Atom>) -> Expr {
    let mut work : Vec<Expr>= Vec::new();
    let mut id:u64 = 0;
    for val in values {
        match val.k() {
            Kind::Ident => {
                match val.op() {
                    Op::Def => {
                        let args = Box::new(work.pop().unwrap());
                        let name = "TODO!".to_owned();
                        let body = Box::new(work.pop().unwrap());
                        work.push(Expr::FuncDef { id, args, body, name })
                    }
                    Op::AsChars => {
                        work.push(Expr::Binary { id, op: Op::AsChars }); 
                        id += 1;
                    }
                    Op::Join => {
                        work.push(Expr::Binary { id, op: Op::Join }); 
                        id += 1;
                    }
                    Op::Plus => {
                        work.push(Expr::Binary { id, op: Op::Plus }); 
                        id += 1;
                    }
                    Op::Minus => {
                        work.push(Expr::Binary { id, op: Op::Minus }); 
                        id += 1;
                    }
                    Op::Mul => {
                        work.push(Expr::Binary { id, op: Op::Mul }); 
                        id += 1;
                    }
                    Op::Div => {
                        work.push(Expr::Binary { id, op: Op::Div }); 
                        id += 1;
                    }
                    Op::Mod => {
                        work.push(Expr::Binary { id, op: Op::Mod }); 
                        id += 1;
                    }
                    Op::Push => {
                        work.push(Expr::Stackv { id, val: Sv::new(val.k(),val.v()) } );
                        id += 1;
                    }
                    Op::Greater => {
                        work.push(Expr::Binary { id, op: Op::Greater }); 
                        id += 1;
                    }
                    Op::Lesser => {
                        work.push(Expr::Binary { id, op: Op::Lesser }); 
                        id += 1;
                    }
                    Op::Equals => {
                        work.push(Expr::Binary { id, op: Op::Equals }); 
                        id += 1;
                    }
                    Op::If => {
                        let (els,ifs) = (Box::new(work.pop().unwrap()),Box::new(work.pop().unwrap())); 
                        work.push(Expr::If {ifs, els});
                        id += 1;
                    }
                    Op::Repeat => {
                        let body = Box::new(work.pop().unwrap()); 
                        work.push(Expr::Repeat {body});
                        id += 1;
                    }
                    Op::Dump => {
                        work.push(Expr::Unary { id, op: Op::Dump }); 
                        id += 1;
                    }
                    Op::Tally => {
                        work.push(Expr::Unary { id, op: Op::Tally }); 
                        id += 1;
                    }
                    Op::DumpMem => {
                        unimplemented!()
                    }
                    Op::Store => {
                        let temp = Box::new(work.pop().unwrap());
                        work.push(Expr::Store { id, var: temp });
                        id += 1;
                    }
                    Op::Id => {
                        let temp = Box::new(work.pop().unwrap());
                        work.push(Expr::Id { id, var: temp });
                        id += 1;
                    }

                    Op::Pop => {
                        if let Some(var) = work.pop() {
                            work.push(Expr::Pop { id, var:Box::new(var) }); 
                        }
                        id += 1;
                    }
                    Op::Append => {
                        if let Some(var) = work.pop() {
                            work.push(Expr::Append { id, var:Box::new(var) }); 
                        }
                        id += 1;
                    }
                    Op::Deop => {
                        unimplemented!()
                    }
                    
                    e=> {
                        println!("Not implemented: {:?}",e);
                        unimplemented!(); 
                    }
                }
            }
            Kind::Str => {
                work.push(Expr::Stackv{ id, val: Sv::new(val.k(), val.v())});
                id += 1;
            }
            Kind::Int => {
                work.push(Expr::Stackv{ id, val: Sv::new(val.k(), val.v())});
                id += 1;
            }
            Kind::Bool => {
                work.push(Expr::Stackv{ id, val: Sv::new(val.k(), val.v())});
                id += 1;
            }
            Kind::Float => {
                work.push(Expr::Stackv{ id, val: Sv::new(val.k(), val.v())});
                id += 1;
            }
            Kind::ArgList => {
                let values = val.v().split_whitespace().map(str::to_string).collect();
                work.push(Expr::ArgFrame { id, lhv: values });
            }
            Kind::FuncBody => {
                work.push(Expr::FuncBody {body: Box::new(parse(Lexer::new(val.v()).lex())) })
            }
            Kind::Null => {}
        }
    }
    Expr::StackFrame { id, val: work.clone() }
}

pub fn compile(name: String, ir: Expr) {
    let dest = format!("{}.rs",name);
    let (generated,genfuncs) = gen_rust(ir);
    let mut init = format!("
    #[derive(Debug, Clone, PartialOrd, PartialEq)]
    enum Values {{
        Str(String),
        Ident(String),
        Int(i64),
        Uint(u64),
        Float(f32),
        Double(f64),
        Bool(bool),
        Chr(char),
        Byte(u8),
        ByteArena(u64),
    }}
    impl std::fmt::Display for Values {{
        fn fmt(&self,f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {{
            match self {{
                Values::Str(s) => write!(f,\"{{}}\",s),
                Values::Ident(s) => write!(f,\"{{}}\",s),
                Values::Int(s) => write!(f,\"{{}}\",s),
                Values::Uint(s) => write!(f,\"{{}}\",s),
                Values::Float(s) => write!(f,\"{{}}\",s),
                Values::Double(s) => write!(f,\"{{}}\",s),
                Values::Bool(s) => write!(f,\"{{}}\",s),
                Values::Chr(s) => write!(f,\"{{}}\",s),
                Values::Byte(s) => write!(f,\"{{}}\",s),
                Values::ByteArena(s) => write!(f,\"{{}}\",s),
            }}
        }}
    }}
    fn main() {{
        let mut program_stack : Vec<Values> = Vec::new();
        {} 
    }}

",generated);
    for func in genfuncs {
        init = format!("{}\n{}",init,func);
    }
    let mut target = std::fs::File::create(dest).unwrap();
    if let Err(e) = target.write_all(init.as_bytes()) {
        eprintln!("{e}");
    }
}

fn gen_rust(ir:Expr) -> (String,Vec<String>) {
    let mut vars: Vec<String> = Vec::new();
    let mut funcs = Vec::new();
    match ir {
        Expr::Stackv { id, val } => {
            match val.k {
                Kind::Str => {
                    let res = format!("// R{}: Pushing String\nprogram_stack.push(Values::Str(\"{}\"));\n",id,val.v);
                    return (res,vec![]);
                }
                Kind::Int => {
                    let res = format!("// R{}: Pushing Int\nprogram_stack.push(Values::Int({}));\n",id,val.v);
                    return (res,vec![]);
                }
                Kind::Bool => {
                    let res = format!("// R{}: Pushing Bool\nprogram_stack.push(Values::Bool({}));\n ",id,val.v);
                    return (res,vec![]);
                }
                Kind::Ident => {
                    if !vars.contains(&val.v) {
                        let res = format!("// Pushing Ident {1}\nlet r{0}: Vec<Values> = vec![]; \n",id,val.v.clone());
                        vars.push(val.v);
                        return (res,vec![]);
                    } else {
                        return ("".to_owned(),vec![]);
                    } 
                }
                Kind::Float => {
                    let res = format!("// R{}: Pushing Float\n program_stack.push(Values::Double({})); \n",id,val.v);
                    return (res,vec![]);
                }
                _ => unreachable!(),
            }
        }
        Expr::Append { id, var } => {
            let var_handle = gen_rust(*var.clone());
            let variable = match *var {
                Expr::Stackv { id, val } => {
                    val
                }
                _ => {
                    eprintln!("Syntax Error: Can't append to something that isn't a value");
                    std::process::exit(1);
                }
            };
            let res = format!("
                {}
                if let Some(to_append) = program_stack.pop() {{
                    {}.push(to_append);
                }} 
            ",var_handle.0, variable.v);
            return (res,var_handle.1)
        }
        Expr::Pop { id:_, var } => {
            let var_handle = gen_rust(*var.clone());
            let variable = match *var {
                Expr::Stackv { id:_, val } => {
                    val
                }
                _ => {
                    eprintln!("Syntax Error: Can't pop something that isn't a value");
                    std::process::exit(1);
                }
            };
            let res = format!("
                {}
                if let Some(to_pop) = {}.pop() {{
                    program_stack.push(to_append);
                }} 
            ",var_handle.0, variable.v);
            return (res,var_handle.1)
        }
        Expr::Store { id:_, var } => {
            let var_handle = gen_rust(*var.clone());
            let variable = match *var {
                Expr::Stackv { id:_, val } => {
                    val
                }
                _ => {
                    eprintln!("Syntax Error: Can't pop something that isn't a value");
                    std::process::exit(1);
                }
            };
            let res = format!("
                {}
                {}.append(&mut program_stack); 
            ",var_handle.0, variable.v);
            return (res,var_handle.1)

        }
        Expr::Id { id, var } => {
            let var_handle = gen_rust(*var.clone());
            let variable = match *var {
                Expr::Stackv { id:_, val } => {
                    val
                }
                _ => {
                    eprintln!("Syntax Error: Can't pop something that isn't a value");
                    std::process::exit(1);
                }
            };
            let res = format!("
                {}
                let mut tmp_read = {}.clone(); 
                program_stack.append(&mut tmp_read); 
            ",var_handle.0, variable.v);
            return (res,var_handle.1)


        }
        Expr::Unary { id, op } => {
            match op {
                Op::Dump => {
                    let res = format!("
                    // R{0}: Printing
                    for elem in program_stack.clone().iter() {{
                        print!(\"{{}} \",elem);
                    }}
                    println!();
                    ",id);
                    return (res,funcs);
                    }
                Op::Tally => {
                    let res = format!("
                    // Tally
                    program_stack.push(Values::Int(program_stack.len() as i64));
                    ");
                    return (res,funcs);
                }
                _ => unreachable!(),
                }
            }
        Expr::Binary { id, op } => {
            match op {
                Op::Plus => {
                    let res = format!("
                    // R{}: Addition
                    if let Some(arg2) = program_stack.pop() {{
                        if let Some(arg1) = program_stack.pop() {{
                            match arg1 {{
                                Values::Int(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => {{
                                            program_stack.push(Values::Int(a + b))
                                        }}
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a as f64 + b))
                                        }}
                                        e => {{
                                            eprintln!(\"Addition not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                Values::Double(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => program_stack.push(Values::Double(a + b as f64)),
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a + b as f64))
                                        }}
                                        e => {{
                                            eprintln!(\"Product not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                e => {{
                                    eprintln!(\"Addition not supported for: {{:?}}\",e);
                                    std::process::exit(1);
                                }}
                            }}
                        }} else {{
                            eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                            std::process::exit(1);   
                        }}
                    }} else {{
                        eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                        std::process::exit(1);
                    }}
                ",id);
                    return (res,funcs);
                }
                Op::Minus => {
                    let res = format!("
                    // R{}: Sub
                    if let Some(arg2) = program_stack.pop() {{
                        if let Some(arg1) = program_stack.pop() {{
                            match arg1 {{
                                Values::Int(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => {{
                                            program_stack.push(Values::Int(a - b))
                                        }}
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a as f64 - b))
                                        }}
                                        e => {{
                                            eprintln!(\"Subtraction not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                Values::Double(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => program_stack.push(Values::Double(a - b as f64)),
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a - b as f64))
                                        }}
                                        e => {{
                                            eprintln!(\"Product not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                e => {{
                                    eprintln!(\"Subtraction not supported for: {{:?}}\",e);
                                    std::process::exit(1);
                                }}
                            }}
                        }} else {{
                            eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                            std::process::exit(1);   
                        }}
                    }} else {{
                        eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                        std::process::exit(1);
                    }}
                ",id);
                    return (res,funcs);
                }
                Op::Mul => {
                    let res = format!("
                    // R{}: Product
                    if let Some(arg2) = program_stack.pop() {{
                        if let Some(arg1) = program_stack.pop() {{
                            match arg1 {{
                                Values::Int(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => {{
                                            program_stack.push(Values::Int(a * b))
                                        }}
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a as f64 * b))
                                        }}
                                        e => {{
                                            eprintln!(\"Product not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                Values::Double(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => program_stack.push(Values::Double(a * b as f64)),
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a * b as f64))
                                        }}
                                        e => {{
                                            eprintln!(\"Product not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                e => {{
                                    eprintln!(\"Product not supported for: {{:?}}\",e);
                                    std::process::exit(1);
                                }}
                            }}
                        }} else {{
                            eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                            std::process::exit(1);   
                        }}
                    }} else {{
                        eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                        std::process::exit(1);
                    }}
                ",id);
                    return (res,funcs);

                }
                Op::Div => {
                    let res = format!("
                    // R{}: Div
                    if let Some(arg2) = program_stack.pop() {{
                        if let Some(arg1) = program_stack.pop() {{
                            match arg1 {{
                                Values::Int(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => {{
                                            program_stack.push(Values::Int(a / b))
                                        }}
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a as f64 / b))
                                        }}
                                        e => {{
                                            eprintln!(\"Div not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                Values::Double(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => program_stack.push(Values::Double(a / b as f64)),
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a / b as f64))
                                        }}
                                        e => {{
                                            eprintln!(\"Product not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                e => {{
                                    eprintln!(\"Div not supported for: {{:?}}\",e);
                                    std::process::exit(1);
                                }}
                            }}
                        }} else {{
                            eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                            std::process::exit(1);   
                        }}
                    }} else {{
                        eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                        std::process::exit(1);
                    }}
                ",id);
                    return (res,funcs);
                }
                Op::Mod => {
                    let res = format!("
                    // R{}: Mod
                    if let Some(arg2) = program_stack.pop() {{
                        if let Some(arg1) = program_stack.pop() {{
                            match arg1 {{
                                Values::Int(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => {{
                                            program_stack.push(Values::Int(a % b))
                                        }}
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a as f64 % b))
                                        }}
                                        e => {{
                                            eprintln!(\"Mod not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                Values::Double(a) => {{
                                    match arg2 {{
                                        Values::Int(b) => program_stack.push(Values::Double(a % b as f64)),
                                        Values::Double(b) => {{
                                            program_stack.push(Values::Double(a % b as f64))
                                        }}
                                        e => {{
                                            eprintln!(\"Product not supported for: {{:?}}\",e);
                                            std::process::exit(1);
                                        }}
                                    }}
                                }}
                                e => {{
                                    eprintln!(\"Mod not supported for: {{:?}}\",e);
                                    std::process::exit(1);
                                }}
                            }}
                        }} else {{
                            eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                            std::process::exit(1);   
                        }}
                    }} else {{
                        eprintln!(\"RuntimeError: R{0} failed with stackempty (Not Enough Args).\");
                        std::process::exit(1);
                    }}
                ",id);
                    return (res,funcs);                
                }
                Op::Equals => {
                    let res = format!("
                    // Equals
                    if let Some(b2) = program_stack.pop() {{
                        if let Some(b1) = program_stack.pop() {{
                            program_stack.push(Values::Bool(b1 == b2));         
                        }} 
                    }}
                ");
                    return (res,funcs);
                }
                Op::Greater => {
                    let res = format!("
                    // Equals
                    if let Some(b2) = program_stack.pop() {{
                        if let Some(b1) = program_stack.pop() {{
                            match (b1,b2) {{
                                (Values::Int(a),Values::Int(b)) => {{
                                    program_stack.push(Values::Bool(a > b))
                                }}
                                (Values::Double(a),Values::Double(b)) => {{
                                    program_stack.push(Values::Bool(a > b))
                                }}
                                _ => unreachable!(),
                            }}  
                        }} 
                    }}
                ");
                    return (res,funcs);
                }
                Op::Lesser => {
                    let res = format!("
                    // Equals
                    if let Some(b2) = program_stack.pop() {{
                        if let Some(b1) = program_stack.pop() {{
                            match (b1,b2) {{
                                (Values::Int(a),Values::Int(b)) => {{
                                    program_stack.push(Values::Bool(a < b))
                                }}
                                (Values::Double(a),Values::Double(b)) => {{
                                    program_stack.push(Values::Bool(a < b))
                                }}
                                _ => unreachable!(),
                            }}  
                        }} 
                    }}
                ");
                    return (res,funcs);
                }
                
                _ => unreachable!(),
            }
        }
        Expr::If { ifs, els } => {
            let (genifs,mut genfuncs) = gen_rust(*ifs);
            funcs.append(&mut genfuncs);
            let (genels,mut genfuncs) = gen_rust(*els);
            funcs.append(&mut genfuncs);
            let res = format!("
                if let Some(cond) = program_stack.pop() {{
                    match cond {{
                        Values::Bool(s) => {{
                            if s {{
                                {genifs}
                            }} else {{
                                {genels}
                            }}
                        }}
                        _ => {{
                            eprintln(\" Type error: expected bool\");
                            std::process::exit(1);
                        }}
                    }} 
                }}
            ");
            return (res,funcs);
        }
        Expr::Repeat { body } => {
            let (genbody,mut genfunc) = gen_rust(*body);
            funcs.append(&mut genfunc);
            let res = format!("
                if let Some(count) = program_stack.pop() {{
                    match count {{
                        Values::Int(s) => {{
                            for _ in 0..s {{
                                {genbody}
                            }}
                        }}
                        _ => {{
                            eprintln!(\"Repeat Error: repeat counter is not an integer\");
                            std::process::exit(1);
                        }}
                    }}
                }}
            ");
            return (res,funcs);
        }
        Expr::FuncBody { body } => {
            let (genbody, mut genfunc) = gen_rust(*body);
            funcs.append(&mut genfunc);
            return (genbody,funcs);
        }
        Expr::FuncDef { id:_, args, body, name } => {}
        Expr::ArgFrame { id:_, lhv } => {
            let mut res = String::new();
            let fnargs = lhv.chunks(2).map(|x| x.to_vec()).collect::<Vec<Vec<String>>>();
            for pair in fnargs {
                match pair[1].as_str() {
                    ":all" => {
                        res = format!("{}, {}: Vec<Values>",res,pair[0]); 
                    }
                    ":Val" => {
                        res = format!("{},{}: Values",res,pair[0]);
                    }
                    ":Ptr" => {
                        res = format!("{}, {}: Vec<Values>",res,pair[0]); 
                    }
                    _ => unimplemented!(),
                }
            }
            return (res,funcs);
        },
        Expr::StackFrame { id:_, val } => {
            let mut res = String::new();
            for elem in val {
                let (generated,mut genfuncs) = gen_rust(elem);
                funcs.append(&mut genfuncs);
                res = format!("{}\n{}",res,generated);
            }
            return (res,funcs);
        }
        Expr::FuncCall { id, args, name } => {}
        
    }
    (String::new(),vec![])
}


