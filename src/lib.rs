use std::collections::HashMap;
use std::io;

static STD_STRING :&str  = include_str!("../stdlib-cet/stdstring.cet");
static STD_STACK :&str  = include_str!("../stdlib-cet/stdstack.cet");
static STD_NUM :&str  = include_str!("../stdlib-cet/stdnum.cet");
static STD_BOOL:&str = include_str!("../stdlib-cet/stdbool.cet");

#[derive(Clone, Debug, PartialEq)]
pub enum Op {
    /// "+" is the symbol. pops twice from the stack and adds, pushing the result into the Stack.
    Plus,
    Mul,
    Minus,
    Div,
    Mod,
    Push,
    Dump,
    DumpMem,
    Pop,
    Append,
    Deop,
    Store,
    Id,
    Equals,
    Lesser,
    Greater,
    If,
    Tally,
    Join,
    AsChars,
    Def,
    Call,
    Get,
    Set,
    Input,
    Include,
    Use,
    Repeat,
    ParseInt,
    ParseFloat,
    Typ,
    Null,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Kind {
    Ident,
    FuncBody,
    ArgList,
    Str,
    Float,
    Int,
    Bool,
    Null,
}

#[derive(Clone, Debug)]
pub struct Atom {
    kind: Kind,
    act: Op,
    val: String,
}
#[derive(Clone, Debug)]
pub struct Sv {
    //Stack value
    pub k: Kind,
    pub v: String,
}

impl Sv {
    pub fn new(k: Kind, v: String) -> Self {
        Self { k, v }
    }
}
impl Atom {
    fn new(k: Kind, a: Op, v: String) -> Self {
        Self {
            kind: k,
            act: a,
            val: v,
        }
    }
    pub fn v(&self) -> String {
        self.val.to_owned()
    }
    pub fn op(&self) -> Op {
        self.act.to_owned()
    }
    pub fn k(&self) -> Kind {
        self.kind.to_owned()
    }
    fn parse_once(self, env: &mut HashMap<String, Vec<Sv>>, stack: &mut Vec<Sv>) {
        match self.kind {
            Kind::Ident => {
                match self.act {
                    Op::Push => {
                        stack.push(Sv::new(Kind::Ident, self.val));
                    }
                    // Op::Xor => {
                    //     unimplemented!()
                    // }
                    Op::Input => {
                        let to_push = readin();
                        stack.push(Sv::new(Kind::Str, to_push));
                    }
                    Op::Id => {
                        if let Some(ptr) = stack.pop() {
                            if let Some(val) = env.get(&ptr.v) {
                                if ptr.k != Kind::Ident {
                                                eprintln!(
                                                    "Reverting! Type error: Expected Ident, found: {:?}",
                                                ptr.k
                                            );
                                        return ;
                                }
                                stack.append(&mut val.clone());
                            }
                        }
                    }
                    Op::If => {
                            if let (Some(els), Some(ifs), Some(cond)) =
                                (stack.pop(), stack.pop(), stack.pop())
                            {
                                if (cond.k != Kind::Bool)
                                    | (els.k != Kind::FuncBody)
                                    | (ifs.k != Kind::FuncBody)
                                {
                                    return ;
                                }
                                if cond.v == "true" {
                                    parse_all(Lexer::new(ifs.v).lex(), env, stack);
                                } else {
                                    parse_all(Lexer::new(els.v).lex(), env, stack);
                                }
                            } else {
                                return ;
                            }
                    }
                    Op::Repeat => {
                            if let (Some(body), Some(num)) =
                                (stack.pop(), stack.pop())
                            {
                                if (num.k != Kind::Int)
                                    | (body.k != Kind::FuncBody)
                                {
                                    return ;
                                }
                                for _i in 0..num.v.parse::<i128>().unwrap() {
                                    parse_all(Lexer::new(body.v.clone()).lex(), env, stack);
                                }
                            } else {
                                return ;
                            }
                    }
                    Op::Def => {
                            if let Some(args) = stack.pop() {
                                if let Some(name) = stack.pop() {
                                    if let Some(func) = stack.pop() {
                                        if (args.k != Kind::ArgList)
                                            | (name.k != Kind::Ident)
                                            | (func.k != Kind::FuncBody)
                                        {
                                            eprintln!("Reverting! Type error: Expected FuncBody,Ident,ArgList, found: {:?},{:?},{:?}", func.k,name.k,args.k);
                                            return ;
                                        }
                                        env.insert(name.v, vec![func, args]);
                                    }
                                }
                            }
                    }
                    Op::Call => {
                        let mut arglist = Vec::new();
                        let mut body = Vec::new();
                        let mut funcname = String::new();
                        let mut func_args: HashMap<String,Vec<Sv>> = env.clone(); // Env of func, does not merge with main env
                        if let Some(funkname) = stack.pop() {
                            funcname = funkname.v.clone();
                            if let Some(func) = env.get(&funkname.v) {
                                let tmp = func[1].v.split_whitespace().map(|x| x.to_string()).collect::<Vec<String>>();
                                for vl in tmp {
                                    arglist.push(vl);
                                }                                    
                                body.push(func[0].clone());
                                   //println!("Body as assign: {:?}", &body);
                            }
                        }
                        if body.is_empty() {
                            eprintln!("UNDEFINED FUNCTION: {funcname}");
                            return 
                        }
                        //assert!(arglist.len() % 2 == 0, "Call, Def Errors: arglist for function {} has odd lenght",funcname);
                        let arg_pairs = arglist.chunks(2).map(|x| x.to_vec()).collect::<Vec<Vec<String>>>();
                        for argp in arg_pairs.clone() {
                            if func_args.contains_key(&argp[0]) {
                                func_args.insert(argp[0].clone(), vec![]);
                            }
                        }
                        let mut func_a_tmp = func_args.clone();
                        for argp in arg_pairs {
                            match argp[1].as_str() {
                                ":append" => {
                                        if let Some(val) = stack.pop() {
                                            if func_a_tmp.contains_key(&argp[0]) {
                                                if let Some(cont) = func_args.get_mut(&argp[0]) {
                                                    cont.push(val);
                                                }    
                                            } else {
                                                func_args.insert(argp[0].clone(), vec![val]);
                                            }
                                        }
                                }
                                ":all" => {
                                    let mut buf = Vec::new();
                                    buf.append(stack);
                                    func_args.insert(argp[0].clone(), buf);
                                }
                                ":new" => {
                                    func_args.insert(argp[0].clone(), vec![]);
                                }
                                ":rev" => {
                                    if func_a_tmp.contains_key(&argp[0]) {
                                        if let Some(cont) = func_args.get_mut(&argp[0]) {
                                                cont.reverse();
                                        }    
                                    }
                                }
                                e => {
                                    eprintln!("Error: Unknown argument instruction {e}");
                                    return 
                                }
                            }
                            func_a_tmp = func_args.clone();
                        }
                        //println!("Body at use: {:?}", &body);
                        let funkie = &body.clone()[0].v;
                        let mut function_stack = Vec::new();
                        parse_all(Lexer::new(funkie.to_owned()).lex(), &mut func_args, &mut function_stack);
                        stack.append(&mut function_stack);
                    
                       // let mut argv = Vec::new();
                       // let mut argc = 0;
                       // let mut function = String::new();
                       // let mut value = vec![];
                       // let mut saved = Vec::new();
                       // if let Some(stack) = env.get_mut("program stack") {
                       //     if let Some(funkname) = stack.pop() {
                       //         if let Some(func) = tmp.get(&funkname.v) {
                       //             argc = func[1].clone().v.parse::<usize>().unwrap_or(0);
                       //             function = func[0].v.clone();
                       //         }
                       //     }
                       //     for _i in 0..argc {
                       //         if let Some(val) = stack.pop() {
                       //             value.push(val);
                       //         }
                       //     }
                       // }
                       // let val_len = value.len();
                       // for i in 0..argc {
                       //     //println!("{}, {}", argc, i);
                       //     //println!("{}", val_len);
                       //     argv.push(value[val_len - 1 - i].clone());
                       // }
                       // if let Some(stack) = env.get_mut("program stack") {
                       //     saved.append(stack);
                       // }
                       // for i in 0..argc {
                       //     let list = argv[i].clone();
                       //     if let Some(stack) = env.get_mut("program stack") {
                       //         stack.push(list);
                       //     }
                       // }
                       // let atoms = Lexer::new(function).lex();
                       // *env = parse_all(atoms, env.clone(),arguments.clone());
                       // if let Some(stack) = env.get_mut("program stack") {
                       //     let mut tmp = Vec::new();
                       //     tmp.append(stack);
                       //     saved.append(&mut tmp);
                       //     stack.append(&mut saved);
                       // }
                       // for i in 0..argc {
                       //     let reg = format!("reg {}", i);
                       //     env.remove(&reg);
                       // }
                    }
                    Op::Dump => {
                            for elem in stack {
                                print!("{} ", elem.v);
                            }
                            println!();
                            //println!("Stack: {:?}", stack);
                            //println!("Env: {:?}", env.clone());
                    }
                    Op::DumpMem => {
                        println!("{:?}",&env);
                    }
                    //Op::Del => {
                    //    if let Some(stack) = env.get_mut("program stack") {
                    //        stack.pop();
                    //    }
                    //}
                    Op::Include => {
                        if let Some(filepath) = stack.pop() {
                            let file = std::fs::read_to_string(filepath.v);
                            match file {
                                Ok(f) => {
                                    let atoms = Lexer::new(f).lex();
                                    //println!("{:?}",&atoms);
                                    parse_all(atoms, env, stack);                                    
                                }
                                Err(e) => {
                                    eprintln!("ERROR: {}", e);
                                }
                            }
                        }
                    }
                    Op::Append => {
                            if let Some(ptr) = stack.pop() {
                                if let Some(val) = stack.pop() {
                                    if ptr.k != Kind::Ident {
                                        eprintln!(
                                            "Reverting! Type error: expected Ident, found: {:?}",
                                            ptr.k
                                        );
                                        return ;
                                    }
                                    if let Some(cont) = env.get_mut(&ptr.v) {
                                        cont.push(val);
                                    } else {
                                        env.insert(ptr.v,vec![val]);
                                }
                            }
                        }
                    }
                    Op::Pop => {
                        if let Some(ptr) = stack.pop() {
                            if ptr.k != Kind::Ident {
                                eprintln!(
                                    "Reverting! Type error: expected Ident, found: {:?}",
                                    ptr.k
                                );
                                return ;
                            }
                            if let Some(cont) = env.get_mut(&ptr.v) {
                                if let Some(val) = cont.pop() {
                                    stack.push(val);
                                } 
                            } 
                        }
                    }
                    Op::Deop => {
                            if let Some(ptr) = stack.pop() {
                                if ptr.k != Kind::Ident {
                                    eprintln!(
                                        "Reverting! Type error: expected Ident, found: {:?}",
                                        ptr.k
                                    );
                                    return ;
                                }
                                env.remove(&ptr.v);
                            }
                    }
                    Op::Store => {
                            if let Some(ptr) = stack.pop() {
                                if ptr.k != Kind::Ident {
                                    eprintln!(
                                        "Reverting! Type error: expected Ident, found: {:?}",
                                        ptr.k
                                    );
                                    return ;
                                }
                                let mut entry = Vec::new();
                                entry.append(stack);
                                env.insert(ptr.v, entry);
                            }
                    }
                    //Op::Dup => {
                    //    if let Some(stack) = env.get_mut("program stack") {
                    //        if let Some(val) = stack.pop() {
                    //            let cpy = val.clone();
                    //            stack.push(cpy);
                    //            stack.push(val);
                    //        }
                    //    }
                    //}
                    Op::Get => {
                            if let Some(index) = stack.pop() {
                                if index.k != Kind::Int {
                                    eprintln!(
                                        "Reverting! Type error: expected Int, found: {:?}",
                                        index.k
                                    );
                                    return ;
                                }
                                let stacklen = stack.len() - 1;
                                //println!("{}",index.v.clone());
                                let thataddr = stacklen
                                    - index.v.parse::<usize>().expect(
                                        "Fatal error: Get failed due to index not being a Int",
                                    );
                                let mut thatchar = stack[thataddr..=thataddr].to_vec();
                                stack.append(&mut thatchar);
                            }
                    }
                    Op::Set => {
                            if let Some(new_val) = stack.pop() {
                                if let Some(index) = stack.pop() {
                                    if index.k != Kind::Int {
                                        eprintln!(
                                            "Reverting! Type error: expected Int, found: {:?}",
                                            index.k
                                        );
                                        return ;
                                    }
                                    let stacklen = stack.len() - 1;
                                    let thataddr = stacklen
                                        - index.v.parse::<usize>().expect(
                                            "Fatal error: Set failed due to index not being a Int",
                                        );
                                    stack[thataddr] = new_val;
                                }
                            }
                    }
                    Op::Tally => {
                        stack.push(Sv::new(Kind::Int, stack.len().to_string()))
                    }
                    Op::AsChars => {
                            if let Some(str) = stack.pop() {
                                if str.k != Kind::Str {
                                    eprintln!(
                                        "Reverting! Type error: expected Str, found: {:?}",
                                        str.k
                                    );
                                    return ;
                                }
                                let lenght = str.v.len();
                                let mut chars = str
                                    .v
                                    .chars()
                                    .map(|x| Sv::new(Kind::Str, x.to_string()))
                                    .collect();
                                stack.append(&mut chars);
                                stack.push(Sv::new(Kind::Int, lenght.to_string()))
                            }
                    }
                    Op::Join => {
                        let mut result = String::new();
                            if let Some(lenght) = stack.pop() {
                                if lenght.k != Kind::Int {
                                    eprintln!(
                                        "Reverting! Type error: expected Int, found: {:?}",
                                        lenght.k
                                    );
                                    return ;
                                }
                                let lenght = if let Ok(len) = lenght.v.parse::<i32>() {
                                    len
                                } else {
                                    eprintln!("Lenght is not a number! Please let the lenght of the string be on the top of the stack.");
                                    0
                                };
                                for _i in 0..lenght {
                                    if let Some(val) = stack.pop() {
                                        result = format!("{}{}", val.v, result);
                                    } else {
                                        //eprintln!("Warning: Lenght is bigger than string.");
                                        break;
                                    }
                                }
                                stack.push(Sv::new(Kind::Str, result));
                            }
                    }
                    //Op::Not => {
                    //    if let Some(stack) = env.get_mut("program stack") {
                    //        if let Some(val) = stack.pop() {
                    //            if val.k != Kind::Bool {
                    //                eprintln!(
                    //                    "Reverting! Type error: expected type Bool, found: {:?}",
                    //                    val.k
                    //                );
                    //                return ;
                    //            }
                    //            match val.v.as_ref() {
                    //                "true" => stack.push(Sv::new(Kind::Bool, "false".to_owned())),
                    //                "false" => stack.push(Sv::new(Kind::Bool, "true".to_owned())),
                    //                e => {
                    //                    eprintln!(
                    //                        "Keyword error: Type is Bool, found word: {} ",
                    //                        e
                    //                    );
                    //                    return ;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    Op::ParseInt => {
                            if let Some(val) = stack.pop() {
                                match val.v.parse::<i128>() {
                                    Ok(o) => stack.push(Sv::new(Kind::Int, o.to_string())),
                                    Err(_) => return ,
                                }
                            }
                    }
                    Op::ParseFloat => {
                            if let Some(val) = stack.pop() {
                                match val.v.parse::<f64>() {
                                    Ok(o) => stack.push(Sv::new(Kind::Float, o.to_string())),
                                    Err(_) => return ,
                                }
                            }
                    }
                    //Op::And => {
                    //    if let Some(stack) = env.get_mut("program stack") {
                    //        if let Some(val1) = stack.pop() {
                    //            if let Some(val2) = stack.pop() {
                    //                if (val1.k != Kind::Bool) | (val2.k != Kind::Bool) {
                    //                    eprintln!("Reverting! Type error: expected Bool Bool, found: {:?} {:?}", val1.k, val2.k);
                    //                    return ;
                    //                }
                    //                let val2: bool = val2.v.parse().unwrap();
                    //                let val1: bool = val1.v.parse().unwrap();
                    //                if val2 & val1 {
                    //                    stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                    //                } else {
                    //                    stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    //Op::Or => {
                    //    if let Some(stack) = env.get_mut("program stack") {
                    //        if let Some(val1) = stack.pop() {
                    //            if let Some(val2) = stack.pop() {
                    //                if (val1.k != Kind::Bool) | (val2.k != Kind::Bool) {
                    //                    eprintln!("Reverting! Type error: expected Bool Bool, found: {:?} {:?}", val1.k, val2.k);
                    //                    return ;
                    //                }
                    //                let val2: bool = val2.v.parse().unwrap();
                    //                let val1: bool = val1.v.parse().unwrap();
                    //                if val2 | val1 {
                    //                    stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                    //                } else {
                    //                    stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    Op::Equals => {
                            if let Some(val1) = stack.pop() {
                                if let Some(val2) = stack.pop() {
                                    if val1.k != val2.k {
                                        eprintln!("Reverting! Type error: expected equal types, found: {:?} {:?}", val1.k, val2.k);
                                        return ;
                                    }
                                    if val2.v == val1.v {
                                        stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                                    } else {
                                        stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                                    }
                                }
                            }
                    }
                    Op::Greater => {
                            if let Some(val1) = stack.pop() {
                                if let Some(val2) = stack.pop() {
                                    match (&val1.k,&val2.k) {
                                        (Kind::Int,Kind::Int) => {
                                                let val2: i128 = val2.v.parse().unwrap();
                                                let val1: i128 = val1.v.parse().unwrap();
                                                if val2 > val1 {
                                                    stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                                                } else {
                                                    stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                                                }
                                            }
                                        (Kind::Float,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            if val2 > val1 {
                                                stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                                            } else {
                                                stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                                            }
                                        },
                                        (Kind::Int,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            if val2 > val1 {
                                                stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                                            } else {
                                                stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                                            }
                                        }
                                        (Kind::Float,Kind::Int) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            if val2 > val1 {
                                                stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                                            } else {
                                                stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                                            }
                                        },
                                        _ => {
                                            eprintln!("Reverting! Type error: expected Num Num, found: {:?} {:?}", val1.k, val2.k);
                                            return ;
                                        }
                                    }
                                }
                            }
                    }
                    Op::Lesser => {
                            if let Some(val1) = stack.pop() {
                                if let Some(val2) = stack.pop() {
                                    match (&val1.k,&val2.k) {
                                        (Kind::Int,Kind::Int) => {
                                                let val2: i128 = val2.v.parse().unwrap();
                                                let val1: i128 = val1.v.parse().unwrap();
                                                if val2 < val1 {
                                                    stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                                                } else {
                                                    stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                                                }
                                            }
                                        (Kind::Float,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            if val2 < val1 {
                                                stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                                            } else {
                                                stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                                            }
                                        },
                                        (Kind::Int,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            if val2 < val1 {
                                                stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                                            } else {
                                                stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                                            }
                                        }
                                        (Kind::Float,Kind::Int) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            if val2 < val1 {
                                                stack.push(Sv::new(Kind::Bool, "true".to_owned()));
                                            } else {
                                                stack.push(Sv::new(Kind::Bool, "false".to_owned()));
                                            }
                                        },
                                        _ => {
                                            eprintln!("Reverting! Type error: expected Num Num, found: {:?} {:?}", val1.k, val2.k);
                                            return ;
                                        }
                                    }
                                }
                            }
                    }
                    Op::Plus => {
                            if let Some(val1) = stack.pop() {
                                if let Some(val2) = stack.pop() {
                                    match (&val1.k,&val2.k) {
                                        (Kind::Int,Kind::Int) => {
                                            let val2: i128 = val2.v.parse().unwrap();
                                            let val1: i128 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Int, (val2+val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2+val1).to_string()));
                                        },
                                        (Kind::Int,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2+val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Int) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2+val1).to_string()));
                                        },
                                        _ => {
                                            eprintln!("Reverting! Type error: expected Num Num, found: {:?} {:?}", val1.k, val2.k);
                                            return ;
                                        }
                                    }
                                }
                            }
                    }
                    Op::Minus => {
                            if let Some(val1) = stack.pop() {
                                if let Some(val2) = stack.pop() {
                                    match (&val1.k,&val2.k) {
                                        (Kind::Int,Kind::Int) => {
                                            let val2: i128 = val2.v.parse().unwrap();
                                            let val1: i128 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Int, (val2-val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2-val1).to_string()));
                                        },
                                        (Kind::Int,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2-val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Int) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2-val1).to_string()));
                                        },
                                        _ => {
                                            eprintln!("Reverting! Type error: expected Num Num, found: {:?} {:?}", val1.k, val2.k);
                                            return ;
                                        }
                                    }
                                }
                            }
                    }
                    Op::Mul => {
                            if let Some(val1) = stack.pop() {
                                if let Some(val2) = stack.pop() {
                                    match (&val1.k,&val2.k) {
                                        (Kind::Int,Kind::Int) => {
                                            let val2: i128 = val2.v.parse().unwrap();
                                            let val1: i128 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Int, (val2*val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2*val1).to_string()));
                                        },
                                        (Kind::Int,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2*val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Int) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2*val1).to_string()));
                                        },
                                        _ => {
                                            eprintln!("Reverting! Type error: expected Num Num, found: {:?} {:?}", val1.k, val2.k);
                                            return ;
                                        }
                                    }
                                }
                            }
                    }
                    Op::Div => {
                            if let Some(val1) = stack.pop() {
                                if let Some(val2) = stack.pop() {
                                    match (&val1.k,&val2.k) {
                                        (Kind::Int,Kind::Int) => {
                                            let val2: i128 = val2.v.parse().unwrap();
                                            let val1: i128 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Int, (val2/val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2/val1).to_string()));
                                        },
                                        (Kind::Int,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2/val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Int) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2/val1).to_string()));
                                        },
                                        _ => {
                                            eprintln!("Reverting! Type error: expected Num Num, found: {:?} {:?}", val1.k, val2.k);
                                            return ;
                                        }
                                    }
                                }
                            }
                    }
                    Op::Mod => {
                            if let Some(val1) = stack.pop() {
                                if let Some(val2) = stack.pop() {
                                    match (&val1.k,&val2.k) {
                                        (Kind::Int,Kind::Int) => {
                                            let val2: i128 = val2.v.parse().unwrap();
                                            let val1: i128 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Int, (val2%val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2%val1).to_string()));
                                        },
                                        (Kind::Int,Kind::Float) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2%val1).to_string()));
                                        }
                                        (Kind::Float,Kind::Int) => {
                                            let val2: f64 = val2.v.parse().unwrap();
                                            let val1: f64 = val1.v.parse().unwrap();
                                            stack.push(Sv::new(Kind::Float, (val2%val1).to_string()));
                                        },
                                        _ => {
                                            eprintln!("Reverting! Type error: expected Num Num, found: {:?} {:?}", val1.k, val2.k);
                                            return ;
                                        }
                                    }
                                }
                            }
                    }
                    Op::Use => {
                            if let Some(stdmod) = stack.pop() {
                                match stdmod.v.as_ref() {
                                    "stdnum" => {
                                        parse_all(Lexer::new(STD_NUM.to_owned()).lex(), env, stack);
                                    }
                                    "stdstack" => {
                                        parse_all(Lexer::new(STD_STACK.to_owned()).lex(), env, stack);
                                    }
                                    "stdstring" => {
                                        parse_all(Lexer::new(STD_STRING.to_owned()).lex(), env, stack);
                                    }
                                    "stdbool" => {
                                        parse_all(Lexer::new(STD_BOOL.to_owned()).lex(), env, stack);
                                    }
                                    _ => stack.push(stdmod),
                                }
                            }
                    } 
                    Op::Typ => {
                            if let Some(elem) = stack.pop() {
                                match elem.k {
                                    Kind::Ident => stack.push(Sv::new(Kind::Str, "Ident".to_owned())),
                                    Kind::Str => stack.push(Sv::new(Kind::Str, "Str".to_owned())),
                                    Kind::Int => stack.push(Sv::new(Kind::Str, "Int".to_owned())),
                                    Kind::FuncBody => stack.push(Sv::new(Kind::Str, "FuncBody".to_owned())),
                                    Kind::Bool => stack.push(Sv::new(Kind::Str, "Bool".to_owned())),
                                    Kind::Float => stack.push(Sv::new(Kind::Str, "Float".to_owned())),
                                    Kind::ArgList => stack.push(Sv::new(Kind::Str, "ArgList".to_owned())),
                                    Kind::Null => stack.push(Sv::new(Kind::Str, "Null".to_owned())),
                                }
                            }
                    }
                    //Op::Rev => {
                    //    if let Some(stack) = env.get_mut("program stack") {
                    //        stack.reverse();
                    //    }
                    //}
                    //Op::Drop => {
                    //    if let Some(stack) = env.get_mut("program stack") {
                    //        *stack = vec![];
                    //    }
                    //}
                    Op::Null => {}
                    _ => panic!("NANI DA FOOK"),
                };
            }
            Kind::Str => {
                    stack.push(Sv::new(Kind::Str, self.val));
            }
            Kind::FuncBody => {
                    stack.push(Sv::new(Kind::FuncBody, self.val));
            }
            Kind::ArgList => {
                    stack.push(Sv::new(Kind::ArgList, self.val));
            }
            Kind::Float => {
                    stack.push(Sv::new(Kind::Float, self.val));
            }
            Kind::Int => {
                    stack.push(Sv::new(Kind::Int, self.val));
            }
            Kind::Bool => {
                    stack.push(Sv::new(Kind::Bool, self.val));
            }
            Kind::Null => {} // _ => panic!("H-How?"),
        }
    }
}

pub struct Lexer {
    source: Vec<char>,
    counter: usize,
}

impl Iterator for Lexer {
    type Item = Atom;
    fn next(&mut self) -> Option<Atom> {
        if self.source.len() > self.counter {
            if let Some(c) = self.curr_char() {
                match c {
                    '\"' => {
                        self.counter += 1;
                        let mut buffer = String::new();
                        while self.curr_char() != Some('\"') {
                            if self.curr_char() == Some('\\') {
                                match self.next_char().unwrap_or_default() {
                                    'n' => {
                                        buffer.push('\n');
                                        self.counter += 2;
                                    }
                                    _ => unimplemented!(),
                                }
                            } else {
                                buffer.push(self.curr_char().expect("Unmatched quotes!"));
                                self.counter += 1
                            }
                        }
                        self.counter += 1;
                        Some(Atom::new(Kind::Str, Op::Push, buffer))
                    }
                    '[' => {
                        self.counter += 1;
                        let mut _buffer = String::new();
                        while self.curr_char() != Some(']') {
                            _buffer.push(self.curr_char().expect("Unmatched quotes!"));
                            self.counter += 1
                        }
                        self.counter += 1;
                        Some(Atom::new(Kind::Null, Op::Null, "".to_owned()))
                    }
                    '{' => {
                        self.counter += 1;
                        let mut state = 1;
                        let mut _buffer = String::new();
                        while state != 0 {
                            if self.curr_char() == Some('{') {
                                state += 1;
                            }
                            if self.curr_char() == Some('}') {
                                state -=1;
                            }
                            if state == 0 {
                                break;
                            }
                            _buffer.push(self.curr_char().expect("Unmatched quotes!"));
                            self.counter += 1
                        }
                        self.counter += 1;
                        Some(Atom::new(Kind::FuncBody, Op::Push, _buffer))
                    }
                    '(' => {
                        self.counter += 1;
                        let mut _buffer = String::new();
                        while self.curr_char() != Some(')') {
                            _buffer.push(self.curr_char().expect("Unmatched quotes!"));
                            self.counter += 1
                        }
                        self.counter += 1;
                        Some(Atom::new(Kind::ArgList, Op::Push, _buffer))
                    }

                    '_' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Null, Op::Push, "_".to_owned()))
                    }
                    '+' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Plus, "".to_string()))
                    }
                    '-' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Minus, "".to_string()))
                    }
                    '/' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Div, "".to_string()))
                    }
                    '*' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Mul, "".to_string()))
                    }
                    '#' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Tally, "".to_string()))
                    }
                    '@' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Id, "".to_string()))
                    }
                    '§' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::AsChars, "".to_string()))
                    }
                    '&' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Join, "".to_string()))
                    }
                    ':' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Call, "".to_string()))
                    }
                    '>' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Greater, "".to_string()))
                    }
                    '<' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Lesser, "".to_string()))
                    }
                    '=' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Equals, "".to_string()))
                    }
                    '%' => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Ident, Op::Mod, "".to_string()))
                    }

                    _ if c.is_alphabetic() => {
                        let mut buffer: String = String::new();
                        while self.curr_char().unwrap_or(' ').is_alphanumeric() {
                            buffer.push(self.curr_char().unwrap());
                            self.counter += 1
                        }
                        self.counter += 1;
                        return match buffer.as_ref() {
                            "include" => Some(Atom::new(Kind::Ident, Op::Include, "".to_string())),
                            "repeat" => Some(Atom::new(Kind::Ident, Op::Repeat, "".to_string())),
                            "use" => Some(Atom::new(Kind::Ident, Op::Use, "".to_string())),
                            //"rev" => Some(Atom::new(Kind::Ident, Op::Rev, "".to_string())),
                            "read" => Some(Atom::new(Kind::Ident, Op::Input, "".to_string())),
                            "get" => Some(Atom::new(Kind::Ident, Op::Get, "".to_string())),
                            "set" => Some(Atom::new(Kind::Ident, Op::Set, "".to_string())),
                            "def" => Some(Atom::new(Kind::Ident, Op::Def, "".to_string())),
                            //"drop" => Some(Atom::new(Kind::Ident, Op::Drop, "".to_string())), // backward compatibility
                            //"yeet" => Some(Atom::new(Kind::Ident, Op::Drop, "".to_string())),
                            "print" => Some(Atom::new(Kind::Ident, Op::Dump, "".to_string())),
                            "printEnv" => Some(Atom::new(Kind::Ident, Op::DumpMem, "".to_string())),
                            //"del" => Some(Atom::new(Kind::Ident, Op::Del, "".to_string())),
                            "pop" => Some(Atom::new(Kind::Ident, Op::Pop, "".to_string())),
                            "append" => Some(Atom::new(Kind::Ident, Op::Append, "".to_string())),
                            "deop" => Some(Atom::new(Kind::Ident, Op::Deop, "".to_string())),
                            "store" => Some(Atom::new(Kind::Ident, Op::Store, "".to_string())),
                            //"dup" => Some(Atom::new(Kind::Ident, Op::Dup, "".to_string())),
                            //"dup2" => Some(Atom::new(Kind::Ident, Op::Dup2, "".to_string())),
                            "true" => Some(Atom::new(Kind::Bool, Op::Push, "true".to_owned())),
                            "false" => Some(Atom::new(Kind::Bool, Op::Push, "false".to_owned())),
                            //"not" => Some(Atom::new(Kind::Ident, Op::Not, "".to_string())),
                            //"and" => Some(Atom::new(Kind::Ident, Op::And, "".to_string())),
                            //"or" => Some(Atom::new(Kind::Ident, Op::Or, "".to_string())),
                            "if" => Some(Atom::new(Kind::Ident, Op::If, "".to_string())),
                            "parseInt" => Some(Atom::new(Kind::Ident, Op::ParseInt, "".to_string())),
                            "parseFloat" => Some(Atom::new(Kind::Ident, Op::ParseFloat, "".to_string())),
                            "typeof" => Some(Atom::new(Kind::Ident, Op::Typ, "".to_string())),
                           _ => Some(Atom::new(Kind::Ident, Op::Push, buffer)),
                        };
                    }
                    _ if c.is_numeric() => {
                        let mut buffer = String::new();
                        while self.curr_char().unwrap_or(' ').is_numeric() || self.curr_char().unwrap_or(' ') == '.' {
                            buffer.push(self.curr_char().unwrap());
                            self.counter += 1
                        }
                        self.counter += 1;
                        match buffer.contains('.') {
                            true => Some(Atom::new(Kind::Float,Op::Push,buffer)),
                            false => Some(Atom::new(Kind::Int, Op::Push, buffer))
                        }
                    }

                    _ => {
                        self.counter += 1;
                        Some(Atom::new(Kind::Null, Op::Push, " ".to_owned()))
                    }
                }
            } else {
                Some(Atom::new(Kind::Null, Op::Null, " ".to_owned()))
            }
        } else {
            None
        }
    }
}

impl Lexer {
    pub fn new(contents: String) -> Self {
        Self {
            source: contents.chars().collect(),
            counter: 0,
        }
    }
    pub fn lex(&mut self) -> Vec<Atom> {
        self.into_iter().collect()
    }
    fn curr_char(&self) -> Option<char> {
        self.source.get(self.counter).map(|x| *x)
    }
    fn next_char(&self) -> Option<char> {
        self.source.get(self.counter + 1).map(|x| *x)
    }
}

pub fn parse_all(atoms: Vec<Atom>, env: &mut HashMap<String, Vec<Sv>>, stack: &mut Vec<Sv>) {
    for elem in atoms {
        elem.parse_once(env, stack);
    }
}

pub fn readin() -> String {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("error: couldn't get input!");

    return input.trim().to_string();
}

