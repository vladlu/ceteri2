mod lib;
mod com;
use lib::{readin, Lexer};
use std::{collections::HashMap, io::Write};

fn main() {
    let mut env: HashMap<String, Vec<lib::Sv>> = HashMap::new();
    let mut program_stack : Vec<lib::Sv> = Vec::new();
    let argv: Vec<String> = std::env::args().collect();
    let argc = argv.len();
    if argc > 1 {
        match argv[1].as_ref() {
            "-h" | "--help" => println!("Usage: ceteri2 [option]\n Options:\n  Nothing -> runs REPL\n  -h | --help -> prints this!\n  <filename> -> interprets file!"),
            "com" => {
                let file = std::fs::read_to_string(argv[2].clone());
                match file {
                    Ok(f) => {
                        let outfilename = argv[2].clone().split(".").next().unwrap().to_owned();
                        let values = com::parse(Lexer::new(f).lex());
                        println!("{:#?}", &values);
                        com::compile(outfilename.clone(),values);
                        let finalname = format!("{}.rs",outfilename);
                        std::process::Command::new("rustc")
                            .args(&["-O",&finalname])
                            .output()
                            .unwrap();
                        //std::process::Command::new("rm")
                        //    .arg(&finalname)
                        //    .output()
                        //    .unwrap();
                    }
                    Err(e) => {eprintln!("Error:{}",e); std::process::exit(1)}
                }
            }
            _ => {
                let file = std::fs::read_to_string(argv[1].clone());
                match file {
                    Ok(f) => {
                        let atoms = Lexer::new(f).lex();
                        // println!("{:?}",&atoms);
                        lib::parse_all(atoms, &mut env, &mut program_stack);
                    }
                    Err(e) => {eprintln!("ERROR: {}",e); std::process::exit(1)}
                }
            }
        }
    } else {
        loop {
            print!("vis2_>  ");
            if std::io::stdout().flush().is_err() {
                panic!("Console unreachable!");
            }
            let input = readin();
            if &input == "quit" {
                println!(" Quitting...");
                std::process::exit(0);
            }
            let atoms = Lexer::new(input).lex();
            //println!("{:?}", atoms.clone());
            lib::parse_all(atoms, &mut env, &mut program_stack);
        }
    }
}
