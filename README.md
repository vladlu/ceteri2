# Outdated. New version todo:

- TODO: Fix Documentation
- TODO: Merge Env and Arguments hashmap for function universality
# Language Spec

## Introduction
Ceteri is a stack-based, concatenative, interpreted language (with some effort into making it compiled in the past, but for now that feature is suspended).
It was inspired by Porth by tsoding (https://gitlab.com/tsoding/porth), but takes no current resemblance.
This repo is the original implementation in Rust.

## Data Types (Kind enum)
### Ident
Ident data type is a simple symbol: It can be used as a const string, variable and function names.
### Str
Str data type is a string: a string is any set of characters included between two " characters.
### Int
Int data type contains integers (i128). Coerces to Float.
### Float
Float is a Number data type that is composed only of numbers (f64).
### Bool
Bool data type represents the value of "true" or "false". These cannot be converted or assumed to be numbers.
### FuncBody
Contains code to be evaluated
### ArgList
Contains directives to create arguments - Supported directives: 
- ":append" (pop from stack and push into arg)
- ":store" (store all of the stack into arg)
- ":new" (create new stack and put it into arg)

## Syntax
The simple reference of writing any of the data types (like writing 123 "Hello" Bye) pushes a value into the program stack.
These values are then manipulated using operators and functions. 
This language uses reverse polish notation (https://en.wikipedia.org/wiki/Reverse_Polish_notation), which means an operator or function are referenced AFTER the data.
## Comments
A comment is anything wrapped in []. Example: 
```ceteri
[This is a comment!]
```
## Operators
### Mathematic Operators:
- Plus (+) [adds two elements]
```ceteri
34 35 +         [returns 69]
```
- Minus (-) [subtracts two elements]
```ceteri
100 31 -         [returns 69]
```
- Product (*) [also known as Mul, multiplies two elements]
```ceteri
23 3 *         [returns 69]
```
- Division (/) [also known as Div, divides two elements]
```ceteri
138 2 /         [returns 69]
```
- Remainder (%) [also known as Mod, gets the remainder of a division between two elements]
```ceteri
100 4 %         [returns 0]
```
- Equals (=) [gets two elements and tries for equality, return boolean]
```ceteri
100 99 =         [returns false]
```
- Greater (>) [gets two elements and tries for the first being bigger than the second]
```ceteri
100 99 >         [returns true]
```
- Lesser (<) [oposite of Greater]
```ceteri
100 99 <         [returns false]
```
- Not (not) [gets an element that is a boolean and negates its value]
```ceteri
true not         [returns false]
```
- And (and) [two elements -> applies boolean and]
```ceteri
true false and         [returns false]
```
- Or (or) [two elements -> applies boolean or]
```ceteri
true false or         [returns true]
``` 
### IO Operators
- Dump (print) [dumps the content of the stack to output]
```ceteri
100 99 "Hello" print         [prints 100 99 "Hello"]
```
- Input (read) [reads user input]
```ceteri
read         [returns whatever the user inputs]
```
- Include (include) [usage: <str_with_path> include. Includes the program/library in a file.]
```ceteri
"src/stdlib.cet" include         [functions inside stdlib.cet are now in scope and any code that isn't a function will execute]
```
### Stack Operators (apply to stack)
- Reverse (rev) [reverses the stack element by element]
```ceteri
1 2 3 4 5 6 rev         [returns 6 5 4 3 2 1]
```
- Drop (drop) [deletes the whole stack]
```ceteri
100 99 bla blah BLAH drop        [clears the stack (deprecated)]
[equivalent code]
10 99 bla blah BLAH yeet         [clears the stack]
```
- Store [see "Variable Operators"]
- Tally (#) [returns the number of elements in the stack + 1]
```ceteri
100 99 #         [returns 100 99 2]
```
- Get (get) [usage: <num> get. Gets the element at position num. Mind that the top of the stack is 0.]
```ceteri
23 45 67 89 2 get         [returns 23 45 67 89 45]
```
### Atom Operators (apply to one argument)
- Delete (del) [deletes one element of the stack]
```ceteri
100 99 del         [returns 100]
```
- Deop [see "Variable Operators"]
- Store [see "Variable Operators"]
- Id [see "Variable Operators"]
- Join (&) [usage: <n> &. Joins "n" elements together by the number "n" provided]
```ceteri
"h" "e" "y" 3 &         [returns "hey"]
```
- AsChars (§) [usage: <Str> §. Returns the string character by character and the number of characters.]
```ceteri
"hey" §         [returns "h" "e" "y" 3]
```
- Dup (dup) [usage: <elem> dup. Dupplicates elem at the top of the stack.]
```ceteri
1 dup         [returns 1 1]
```
### Variable Operators (define and manipulate variables)
- Append (append) [usage: <element> <var_name(Ident)> append. Removes one element from the stack and pushes it to var_name.]
```ceteri
12 13 hey append         [returns 12, hey = 13]
```
- Pop (pop) [usage: <element> <var_name(Ident)> pop. Removes the top element from var_name and pushes it the stack.]
```ceteri
12 hey pop         [hey = 13, returns 12 13, hey = None]
```
- Deop (deop) [usage: <var_name> deop. Deletes var_name: it stops holding a variable value.]
```ceteri
100 hey append hey deop         [hey = 100 and then hey = Null]
```
- Store (store) [usage: <var_name> store. Stores all the stack contents into <var_name>]
```ceteri
100 99 hey store         [returns nothing, hey = 100 99]
```
- Id (@) [usage: <var_name> @. Reads contents of var_name and puts them on the stack.]
```ceteri
12 13 14 15 hey store
69 print del
hey @ [prints 69, returns 12 13 14 15, hey = 12 13 14 15]
```
### Control Flow Operators
- If (if) [usage: <cond> <true branch> <false branch> if. Depending on the boolean value of cond, returns one of the branches]
```ceteri
3 2 > {hey} {bye} if print [returns hey, prints hey]
```
- Def (def) [usage: <FuncBody> <Func Name> <Arglist> def. Defines a function based on the code, name, and arguments defined by stack operators.]

```ceteri
{b @ a @ print} printTwo (a :append b :append) def [receives two elements and prints them]
```

- Call (:) [usage: <Ident> : . Calls the contents of ident, obeying all commands on arglist]
```ceteri
12 13 14 printTwo : [prints 13 and 14, returns 12 13 14]
```

- Repeat (repeat) [usage: <FuncBody> int repeat, repeats the funcbody expression int times]
```ceteri
"Hello" {print} 5 repeat [prints Hello 5 times, returns "Hello"]
```

The code is read from left to right, the top of the stack represents the latest added item.
implementation details vary depending on implementation.
