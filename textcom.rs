
    #[derive(Debug, Clone, PartialOrd, PartialEq)]
    enum Values {
        Str(String),
        Ident(String),
        Int(i64),
        Uint(u64),
        Float(f32),
        Double(f64),
        Bool(bool),
        Chr(char),
        Byte(u8),
        ByteArena(u64),
    }
    impl std::fmt::Display for Values {
        fn fmt(&self,f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Values::Str(s) => write!(f,"{}",s),
                Values::Ident(s) => write!(f,"{}",s),
                Values::Int(s) => write!(f,"{}",s),
                Values::Uint(s) => write!(f,"{}",s),
                Values::Float(s) => write!(f,"{}",s),
                Values::Double(s) => write!(f,"{}",s),
                Values::Bool(s) => write!(f,"{}",s),
                Values::Chr(s) => write!(f,"{}",s),
                Values::Byte(s) => write!(f,"{}",s),
                Values::ByteArena(s) => write!(f,"{}",s),
            }
        }
    }
    fn main() {
        let mut program_stack : Vec<Values> = Vec::new();
        
// R0: Pushing Int
program_stack.push(Values::Int(5));

// R1: Pushing Int
program_stack.push(Values::Int(5));

// R2: Pushing Int
program_stack.push(Values::Int(5));

// R3: Pushing Int
program_stack.push(Values::Int(5));


                    // Tally
                    program_stack.push(Values::Int(program_stack.len() as i64));
                    
// R5: Pushing Int
program_stack.push(Values::Int(1));


                    // R6: Sub
                    if let Some(arg2) = program_stack.pop() {
                        if let Some(arg1) = program_stack.pop() {
                            match arg1 {
                                Values::Int(a) => {
                                    match arg2 {
                                        Values::Int(b) => {
                                            program_stack.push(Values::Int(a - b))
                                        }
                                        Values::Double(b) => {
                                            program_stack.push(Values::Double(a as f64 - b))
                                        }
                                        e => {
                                            eprintln!("Subtraction not supported for: {:?}",e);
                                            std::process::exit(1);
                                        }
                                    }
                                }
                                Values::Double(a) => {
                                    match arg2 {
                                        Values::Int(b) => program_stack.push(Values::Double(a - b as f64)),
                                        Values::Double(b) => {
                                            program_stack.push(Values::Double(a - b as f64))
                                        }
                                        e => {
                                            eprintln!("Product not supported for: {:?}",e);
                                            std::process::exit(1);
                                        }
                                    }
                                }
                                e => {
                                    eprintln!("Subtraction not supported for: {:?}",e);
                                    std::process::exit(1);
                                }
                            }
                        } else {
                            eprintln!("RuntimeError: R6 failed with stackempty (Not Enough Args).");
                            std::process::exit(1);   
                        }
                    } else {
                        eprintln!("RuntimeError: R6 failed with stackempty (Not Enough Args).");
                        std::process::exit(1);
                    }
                

                if let Some(count) = program_stack.pop() {
                    match count {
                        Values::Int(s) => {
                            for _ in 0..s {
                                

                    // R0: Addition
                    if let Some(arg2) = program_stack.pop() {
                        if let Some(arg1) = program_stack.pop() {
                            match arg1 {
                                Values::Int(a) => {
                                    match arg2 {
                                        Values::Int(b) => {
                                            program_stack.push(Values::Int(a + b))
                                        }
                                        Values::Double(b) => {
                                            program_stack.push(Values::Double(a as f64 + b))
                                        }
                                        e => {
                                            eprintln!("Addition not supported for: {:?}",e);
                                            std::process::exit(1);
                                        }
                                    }
                                }
                                Values::Double(a) => {
                                    match arg2 {
                                        Values::Int(b) => program_stack.push(Values::Double(a + b as f64)),
                                        Values::Double(b) => {
                                            program_stack.push(Values::Double(a + b as f64))
                                        }
                                        e => {
                                            eprintln!("Product not supported for: {:?}",e);
                                            std::process::exit(1);
                                        }
                                    }
                                }
                                e => {
                                    eprintln!("Addition not supported for: {:?}",e);
                                    std::process::exit(1);
                                }
                            }
                        } else {
                            eprintln!("RuntimeError: R0 failed with stackempty (Not Enough Args).");
                            std::process::exit(1);   
                        }
                    } else {
                        eprintln!("RuntimeError: R0 failed with stackempty (Not Enough Args).");
                        std::process::exit(1);
                    }
                
                            }
                        }
                        _ => {
                            eprintln!("Repeat Error: repeat counter is not an integer");
                            std::process::exit(1);
                        }
                    }
                }
            

                    // R8: Printing
                    for elem in program_stack.clone().iter() {
                        print!("{} ",elem);
                    }
                    println!();
                     
    }

